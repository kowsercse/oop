package tutorial.oop.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tutorial.oop.ride.sharing.BasicCostCalculator;
import tutorial.oop.ride.sharing.CompositeCostCalculator;
import tutorial.oop.ride.sharing.CostCalculator;

import java.util.ArrayList;

@Configuration
public class AppConfiguration {

  @Bean
  public void x() {
    ArrayList<CostCalculator> costCalculators = new ArrayList<>();
    costCalculators.add(new BasicCostCalculator(15.0, 2, 5));
    new CompositeCostCalculator(costCalculators);
  }

}
