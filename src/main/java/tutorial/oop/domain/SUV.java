package tutorial.oop.domain;

public class SUV extends Vehicle {
  public SUV(int totalSeat, String model) {
    super(totalSeat, model);
  }
}
