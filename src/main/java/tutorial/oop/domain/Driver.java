package tutorial.oop.domain;

import java.time.LocalDate;

public class Driver {
  private String name;
  private LocalDate birthDate;
  private String drivingLicense;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public LocalDate getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }
}
