package tutorial.oop.domain;

public class Sedan extends Vehicle {
  public Sedan(String model) {
    super(5, model);
  }
}
