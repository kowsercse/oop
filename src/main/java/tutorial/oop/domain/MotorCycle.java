package tutorial.oop.domain;

public class MotorCycle extends Vehicle {

  public MotorCycle(String model) {
    super(2, model);
  }
}
