package tutorial.oop.domain;

public class Vehicle {
  private int totalSeat;
  private String licenseNumber;
  private int speed;
  private Driver driver;
  private String model;

  public Vehicle(int totalSeat, String model) {
    this.totalSeat = totalSeat;
    this.model = model;
  }

  public int getTotalSeat() {
    return totalSeat;
  }

  public String getLicenseNumber() {
    return licenseNumber;
  }

  public void setLicenseNumber(String licenseNumber) {
    this.licenseNumber = licenseNumber;
  }

  public int getSpeed() {
    return speed;
  }

  public void setSpeed(int speed) {
    this.speed = speed;
  }

  public Driver getDriver() {
    return driver;
  }

  public void setDriver(Driver driver) {
    this.driver = driver;
  }
}
