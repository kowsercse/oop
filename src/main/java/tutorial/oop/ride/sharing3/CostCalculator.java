package tutorial.oop.ride.sharing3;

import tutorial.oop.domain.*;

/**
 * I just removed usage of Class from your @tutorial.oop.ride.sharing2.CostCalculator
 * I guess, it just easy to explain with instanceof to the students
 * The use of Class class requires explaining a little bit reflection
 * */

public class CostCalculator {
	public <T extends Vehicle> double getCost(T vehicle, double distance) {
		if (vehicle instanceof MotorCycle) {
			double cost = 40;
			double remainingDistance = distance - 2;
			if (remainingDistance > 2) {
				cost += remainingDistance * 10;
			}
			return cost;
		} else if (vehicle instanceof AutoRickshaw) {
			double cost = 50;
			double remainingDistance = distance - 2;
			if (remainingDistance > 2) {
				cost += remainingDistance * 15;
			}
			return cost;
		} else if (vehicle instanceof Sedan) {
			double cost = 70;
			double remainingDistance = distance - 2;
			if (remainingDistance > 2) {
				cost += remainingDistance * 15;
			}
			return cost;
		} else if (vehicle instanceof SUV) {
			double cost = 80;
			double remainingDistance = distance - 2;
			if (remainingDistance > 2) {
				cost += remainingDistance * 15;
			}
			return cost;
		}

		throw new IllegalArgumentException("Vehicle not supported");
	}
}
