package tutorial.oop.ride.sharing2;

import tutorial.oop.domain.*;

public class CostCalculator {

  public <T extends Vehicle> double getCost(Class<T> clazz, double distance) {
    if (MotorCycle.class.equals(clazz)) {
      double cost = 40;
      double remainingDistance = distance - 2;
      if (remainingDistance > 2) {
        cost += remainingDistance * 10;
      }
      return cost;
    } else if (AutoRickshaw.class.equals(clazz)) {
      double cost = 50;
      double remainingDistance = distance - 2;
      if (remainingDistance > 2) {
        cost += remainingDistance * 15;
      }
      return cost;
    } else if (Sedan.class.equals(clazz)) {
      double cost = 70;
      double remainingDistance = distance - 2;
      if (remainingDistance > 2) {
        cost += remainingDistance * 15;
      }
      return cost;
    } else if (SUV.class.equals(clazz)) {
      double cost = 80;
      double remainingDistance = distance - 2;
      if (remainingDistance > 2) {
        cost += remainingDistance * 15;
      }
      return cost;
    }

    throw new IllegalArgumentException("Vehicle not supported");
  }
}
