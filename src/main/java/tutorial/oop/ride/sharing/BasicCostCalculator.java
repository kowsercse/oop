package tutorial.oop.ride.sharing;

public class BasicCostCalculator implements CostCalculator {

  private final double baseCost;
  private final double baseDistance;
  private final double costPerKm;

  public BasicCostCalculator(double baseCost, double baseDistance, double costPerKm) {
    this.baseCost = baseCost;
    this.baseDistance = baseDistance;
    this.costPerKm = costPerKm;
  }

  @Override
  public double getCost(double distance) {
    double remainingDistance = distance - baseDistance;
    return remainingDistance > baseDistance ? baseCost + remainingDistance * costPerKm : baseCost;
  }
}
