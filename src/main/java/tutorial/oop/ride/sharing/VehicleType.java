package tutorial.oop.ride.sharing;

public enum VehicleType {
  SEDAN, MOTOR_CYCLE, RICKSHAW, AUTO_RICKSHAW;
}
