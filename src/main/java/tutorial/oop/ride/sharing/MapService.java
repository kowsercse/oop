package tutorial.oop.ride.sharing;

public interface MapService {
  int getDistance(String start, String end);
}
