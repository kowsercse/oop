package tutorial.oop.ride.sharing;

public class CouponCostCalculator implements CostCalculator {

  private final double couponPrice;

  public CouponCostCalculator(double couponPrice) {
    if (Double.compare(0, couponPrice) >= 0) {
      throw new IllegalArgumentException("Coupon price should not be more than: 0");
    }

    this.couponPrice = couponPrice;
  }

  @Override
  public double getCost(double distance) {
    return -couponPrice;
  }
}
