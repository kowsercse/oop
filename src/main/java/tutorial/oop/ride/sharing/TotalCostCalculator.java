package tutorial.oop.ride.sharing;

import java.util.List;

public class TotalCostCalculator implements CostCalculator {

  private final List<CostCalculator> costCalculators;

  public TotalCostCalculator(List<CostCalculator> costCalculators) {
    this.costCalculators = costCalculators;
  }

  @Override
  public double getCost(double distance) {
    double sum = 0.0;
    for (CostCalculator costCalculator : costCalculators) {
      double cost = costCalculator.getCost(distance);
      sum += cost;
    }
    return sum;
  }
}
