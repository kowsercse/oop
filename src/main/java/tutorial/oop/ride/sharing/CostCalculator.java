package tutorial.oop.ride.sharing;

public interface CostCalculator {

  double getCost(double distance);

}
