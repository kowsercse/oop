package tutorial.oop.ride.sharing;

import java.util.StringJoiner;

public class RideChoice {

  private final double cost;
  private final VehicleType vehicleType;

  public RideChoice(VehicleType vehicleType, double cost) {
    this.vehicleType = vehicleType;
    this.cost = cost;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", RideChoice.class.getSimpleName() + "[", "]")
        .add("cost=" + cost)
        .add("vehicleType=" + vehicleType)
        .toString();
  }
}
