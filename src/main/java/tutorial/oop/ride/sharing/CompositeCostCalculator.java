package tutorial.oop.ride.sharing;

import java.util.List;

public class CompositeCostCalculator implements CostCalculator {

  private final List<CostCalculator> costCalculators;

  public CompositeCostCalculator(List<CostCalculator> costCalculators) {
    this.costCalculators = costCalculators;
  }

  @Override
  public double getCost(double distance) {
    return costCalculators.stream().mapToDouble(costCalculator -> costCalculator.getCost(distance)).sum();
  }
}
