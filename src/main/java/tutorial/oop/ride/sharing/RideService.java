package tutorial.oop.ride.sharing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class RideService {

  private final MapService mapService;
  private final Map<VehicleType, CostCalculator> costCalculatorsByVehicleType;

  @Autowired
  public RideService(MapService mapService, Map<VehicleType, CostCalculator> costCalculatorsByVehicleType) {
    this.costCalculatorsByVehicleType = costCalculatorsByVehicleType;
    this.mapService = mapService;
  }

  public List<RideChoice> findRides(String start, String end) {
    int distance = mapService.getDistance(start, end);

    return costCalculatorsByVehicleType.entrySet().stream()
        .map(entry -> new RideChoice(entry.getKey(), entry.getValue().getCost(distance)))
        .collect(Collectors.toList());
  }

}
