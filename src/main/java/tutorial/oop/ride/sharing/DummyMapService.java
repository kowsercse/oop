package tutorial.oop.ride.sharing;

import java.util.Map;

public class DummyMapService implements MapService {
  private final Map<String, Integer> addressByLocation;

  public DummyMapService(Map<String, Integer> addressByLocation) {
    this.addressByLocation = addressByLocation;
  }

  @Override
  public int getDistance(String start, String end) {
    if (!addressByLocation.containsKey(start) || !addressByLocation.containsKey(end)) {
      throw new IllegalArgumentException("Invalid start or end location");
    }

    return Math.abs(addressByLocation.get(start) - addressByLocation.get(end));
  }
}
